# JavaSE

Runnable jar application that reads in filepath from command line (declare filepath too if needed) and saves it sorted contents in output file, with name provided in command line.

# Compilation

```bash
mvn package && java -jar target/JavaSE-JavaSE.jar filePathToRead newFilePath
```

# Run tests

Make sure to run test in JavaSE directory
```bash
mvn test -Dtest=AppTest
```
# Author
Arina Solovjova
ari.solovjova@gmail.com
package JavaSE;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;

public class AppTest {
    @Test
    public void readTextFile_FileExists_FileNotNull() {
        FileHelper fh = new FileHelper();
        String fileName = "./src/test/resources/readTextFile_FileExists/testfile.txt";

        File file = fh.readTextFile(fileName);
        assertNotNull(file);
    }

    @Test
    public void readTextFile_FileNonExistent_Null() {
        FileHelper fh = new FileHelper();
        String fileName = "noFile.txt";

        File file;
        file = fh.readTextFile(fileName);
        assertNull(file);
    }

    @Test
    public void getSortedFileContent_SortedContent_True() {
        FileHelper fh = new FileHelper();
        String fileName = "./src/test/resources/readTextFile_FileExists/testfile.txt";
        File file = fh.readTextFile(fileName);

        ArrayList<String> sorted = fh.getSortedFileContent(file);
        ArrayList<String> expected = new ArrayList<>();
        expected.add("a");
        expected.add("b");
        expected.add("c");

        assertArrayEquals(expected.toArray(), sorted.toArray());
    }

    @Test
    public void createNewFile_FileCreated_True() {
        FileHelper fh = new FileHelper();
        String filePath = "./src/test/resources/createNewFile_FileCreated/testfile.txt";
        fh.createNewFile(filePath, null);
        
        File file = new File(filePath);
        assertTrue(file.exists());
        
        file.delete();
    }
}
package JavaSE;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class App {
    /**
     * Main entry point for task 1
     * 
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        if (args.length > 0) {
            String filePath = args[0];
            FileHelper fh = new FileHelper();
            File textFile = fh.readTextFile(filePath);
            if (textFile != null) {
                ArrayList<String> content = fh.getSortedFileContent(textFile);
                String newFilePath = args[1];

                fh.createNewFile(newFilePath, content);
            }
        } else {
            System.out.println("No file name arguments given");
        }
    }
}

package JavaSE;

import java.io.File;
import java.util.ArrayList;
import java.io.FileWriter;
import java.io.FileReader;
import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.security.cert.Extension;

/**
 * Helper class with file and file content processing
 * 
 * @author Arina Solovjova
 */
public class FileHelper {

    /**
     * Get file with the given file name
     * 
     * @param fileName Name of file with path to it from project directory
     * @return File if it exists
     */
    public File readTextFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            return file;
        } else {
            System.out.println("No file named '" + file.getAbsolutePath() + "' was found");
            return null;
        }
    }

    /**
     * Creates new file with given file name and path, and content
     * 
     * @param filePath Name and path for new file
     * @param content  New file content
     * @throws IOException
     */
    public void createNewFile(String filePath, ArrayList<String> content) {
        if (filePath != null) {
            try (FileWriter fileWr = new FileWriter(filePath)) {
                if (content != null) {
                    for (String s : content) {
                        fileWr.write(s);
                        fileWr.write("\r\n");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets and sorts file content
     * 
     * @param file File to get content from
     * @return ArrayList<String> of sorted file content if file exists
     * @throws IOException
     */
    public ArrayList<String> getSortedFileContent(File file) {
        if (file.exists()) {
            try (FileReader fr = new FileReader(file.getAbsolutePath())) {
                BufferedReader reader = new BufferedReader(fr);

                ArrayList<String> content = new ArrayList<String>();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    content.add(line);
                }
                Collections.sort(content);
                return content;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
